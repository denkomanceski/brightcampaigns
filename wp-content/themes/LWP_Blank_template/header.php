<?php 
	$bc = redux_post_meta( THEME_OPT, get_the_ID() );
	global $reduxConfig;
?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
	
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">
  		<meta name="author" content="">
		<link rel="shortcut icon" href="images/LogoBrightlogo.ico">
		
		<?php wp_head(); ?>

	</head>

	<body <?php body_class(); ?>>

		<header id="home" class="header" role="banner">
			<div id="home-slider" class="carousel slide carousel-fade" data-ride="carousel">
				<div class="carousel-inner">
					<div class="item active" style="background-color:<?php echo $bc['opt-color-rgba']['rgba']; ?>;">
						<div class="caption">
							<h1 class="animated fadeInLeftBig" style="font-size:<?php echo $bc['h1fontsize']; ?>; font-family:<?php echo $bc['h1fontfamily']; ?>;"><?php echo $bc['header-title']; ?> <span id = "typist-elementone"><?php echo $bc['header-description']; ?></span></h1>
							<a data-scroll class="btn btn-start animated fadeInUpBig" href="<?php echo $bc['header-btn-link']; ?>"><?php echo $bc['header-btn']; ?></a>
						</div>
					</div>
				</div>
				<a id="tohash" href="<?php echo $bc['header-link-tohash']; ?>"><i class="fa fa-angle-down"></i></a>
			</div><!--/#home-slider-->

			<div class="main-nav">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
							<h1>
								<img class="img-responsive" src="<?php echo $bc['header-logo']['url']; ?>" alt="logo">
							</h1>
						</a>                    
					</div>
					
						<?php lwp_nav(); ?>
				</div>
			</div><!--/#main-nav-->

		</header><!--/#home-->				