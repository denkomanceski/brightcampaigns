<?php
if ( function_exists( 'add_theme_support' ) ) { 
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 280, 210, true ); // Normal post thumbnails
    add_image_size( 'screen-shot', 720, 540 ); // Full size screen
}

add_action('init', 'portfolio_register');  
   
function portfolio_register() {  
    $args = array(  
        'label'           => __('Portfolio'),  
        'singular_label'  => __('Project'),  
        'public'          => true,  
        'show_ui'         => true,  
        'capability_type' => 'post',  
        'hierarchical'    => false,  
        'rewrite'         => true,
        'taxonomies'      => array('category'),  
        'supports'        => array('title', 'editor', 'thumbnail')  
       );  
   
    register_post_type( 'portfolio' , $args );  
}

?>