		
<?php
/*
 * Template Name: bchome-page
 */ 
global $bc;?>
		<footer id="footer" class="footer" role="contentinfo">
		    <div class="footer-top wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">   
		      <div class="container text-center">

		        <div class="footer-logo">
		          <a href="#"><img class="img-responsive" src="<?php echo $bc['contact-background']['url']; ?>" alt=""></a>
		        </div>
		        <div class="social-icons">
		          <ul>         
		            <li><a class="envelope" href="mailto:<?php echo $bc['footer-mailto']; ?>"><i class="fa fa-envelope"></i></a></li>
		            <li><a class="skype" href="skype:<?php echo $bc['footer-skype']; ?>"><i class="fa fa-skype"></i></a></li>
		          </ul>
		        </div>
		      </div>
		    </div>
		    <div class="footer-bottom">
		      <div class="container">
		        <div class="row">
		          <div class="col-sm-6">
		            <p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
		          </div>
		          <div class="col-sm-6">
		            <p class="pull-right" style="float:right;"><a href="#">  Algemene voorwaarden  </a></p>
		            <p class="pull-right" style="float:right;"><a href="#">  Gebruiks voowaarden | </a></p>
		            <p class="pull-right" style="float:right;"><a href="#">  Privacy verklaring |  </a></p>
		          </div>
		          <div class="col-sm-12">
		          	<?php get_sidebar('footer'); ?>
		          </div>
		        </div>
		      </div>
		    </div>
		  </footer>
  			<!-- /footer footer-bg -->
  			
		<?php wp_footer(); ?>
	</body>
</html>