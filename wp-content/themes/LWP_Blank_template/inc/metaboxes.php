<?php

// INCLUDE THIS BEFORE you load your ReduxFramework object config file.


// You may replace $redux_opt_name with a string if you wish. If you do so, change loader.php
// as well as all the instances below.
$redux_opt_name = THEME_OPT;

if ( !function_exists( "redux_add_metaboxes" ) ):
	function redux_add_metaboxes($metaboxes) {
	
	$page_options = array();

	$fontstyle = array(
		'title' => 'Font style section',
		'icon_class'    => 'icon-large',
		'icon'          => 'el-icon-list-alt',
		'fields' => array(
			array(
					'id'    => 'h1fontsize',
					'type'  => 'text',
					'title' => __( 'H1 Font Size' ),
					'default' => '60px'
				),
			array(
					'id'    => 'h2fontsize',
					'type'  => 'text',
					'title' => __( 'H2 Font Size' ),
					'default' => '30px'
				),
			array(
					'id'    => 'h3fontsize',
					'type'  => 'text',
					'title' => __( 'H3 Font Size' ),
					'default' => '24px'
				),
			array(
					'id'    => 'h1fontfamily',
					'type'  => 'text',
					'title' => __( 'H1 Font Family' ),
					'default' => "Open Sans', sans-serif"
				),
			array(
					'id'    => 'h2fontfamily',
					'type'  => 'text',
					'title' => __( 'H2 Font Family' ),
					'default' => "'Open Sans', sans-serif"
				),
			array(
					'id'    => 'h3fontfamily',
					'type'  => 'text',
					'title' => __( 'H3 Font Family' ),
					'default' => "'Open Sans', sans-serif"
				),
		)
	);

	$metaHeader = array(
		'title' => 'Header section',
		'icon_class'    => 'icon-large',
		'icon'          => 'el-icon-list-alt',
		'fields' => array(
			array(
					'id'    => 'header-logo',
					'type'  => 'media',
					'title' => __( 'Logo header' )
				),
			array(
				'id'      => 'header-title',
				'type'    => 'text',
				'title'   => __( 'Title' ),
				'default' => 'Grow your business with'
			),
			array(
				'id'      => 'header-description',
				'type'    => 'text',
				'title'   => __( 'Description' ),
				'default' => 'E-mail Marketing'
			),
			array(
				'id'      => 'header-btn',
				'type'    => 'text',
				'title'   => __( 'Button text' ),
				'default' => 'Start now'
			),
			array(
				'id'      => 'header-btn-link',
				'type'    => 'text',
				'title'   => __( 'Button link' ),
				'default' => '#services'
			),
			array(
				'id'      => 'header-link-tohash',
				'type'    => 'text',
				'title'   => __( 'Tohash link' ),
				'default' => '#services'
			),
			array(
			    'id'        => 'opt-color-rgba',
			    'type'      => 'color_rgba',
			    'title'     => 'RGBA Color Picker',
			    'subtitle'  => 'Set color and alpha channel',
			    'desc'      => 'The caption of this button may be changed to whatever you like!',
			 
			    // See Notes below about these lines.
			    //'output'    => array('background-color' => '.site-header'),
			    //'compiler'  => array('color' => '.site-header, .site-footer', 'background-color' => '.nav-bar'),
			    'default'   => array(
			        'color'     => '#fdfdfd',
			        'alpha'     => 1
			    ),
			 
			    // These options display a fully functional color palette.  Omit this argument
			    // for the minimal color picker, and change as desired.
			    'options'       => array(
			        'show_input'                => true,
			        'show_initial'              => true,
			        'show_alpha'                => true,
			        'show_palette'              => true,
			        'show_palette_only'         => false,
			        'show_selection_palette'    => true,
			        'max_palette_size'          => 10,
			        'allow_empty'               => true,
			        'clickout_fires_change'     => false,
			        'choose_text'               => 'Choose',
			        'cancel_text'               => 'Cancel',
			        'show_buttons'              => true,
			        'use_extended_classes'      => true,
			        'palette'                   => null,  // show default
			        'input_text'                => 'Select Color'
			    ),
			),
		)
	);

	$metaServices = array(
		'title' => 'Services section',
		'icon_class'    => 'icon-large',
		'icon'          => 'el-icon-list-alt',
		'fields' => array(
			array(
				'id'      => 'services-title',
				'type'    => 'textarea',
				'title'   => __( 'Title' ),
				'default' => 'BETAALBAAR ALLES-IN-ÉÉN MARKETING PLATFORM'
			),
			array(
				'id'      => 'services-description',
				'type'    => 'textarea',
				'title'   => __( 'Description' ),
				'default' => 'Boost je sales en marketing met de gemakkelijk te gebruiken oplossingen van BrightCampaigns'
			),
			array(
			   'id' => 'section-start',
			   'type' => 'section',
			   'title' => __('Block one' ),
			   'indent' => true 
			),
			array(
				'id'      => 'services-info-title-one',
				'type'    => 'text',
				'title'   => __( 'Title' ),
				'default' => 'E-mail Marketing'
			),
			array(
				'id'      => 'services-info-description-one',
				'type'    => 'textarea',
				'title'   => __( 'Description' ),
				'default' => 'Stuur nieuwsbrieven, ontwerp mooie e-mail campagnes met onze gemakkelijke editor en blijf in contact met je relaties.' 
			),
			array(
				'id'      => 'services-info-btn-one',
				'type'    => 'text',
				'title'   => __( 'Text button' ),
				'default' => 'Learn More'
			),
			array(
				'id'      => 'services-info-btnlink-one',
				'type'    => 'text',
				'title'   => __( 'Link button' ),
				'default' => '#box1'
			),
			array(
			    'id'     => 'section-end',
			    'type'   => 'section',
			    'indent' => false,
			),
			array(
			   'id' => 'section-start',
			   'type' => 'section',
			   'title' => __('Block two' ),
			   'indent' => true 
			),
			array(
				'id'      => 'services-info-title-two',
				'type'    => 'text',
				'title'   => __( 'Title' ),
				'default' => 'Marketing Automation'
			),
			array(
				'id'      => 'services-info-description-two',
				'type'    => 'textarea',
				'title'   => __( 'Description' ),
				'default' => 'Automatiseer je sales en marketing met behulp van gedrags gebaseerde e-mail campagnes, workflows en crm.' 
			),
			array(
				'id'      => 'services-info-btn-two',
				'type'    => 'text',
				'title'   => __( 'Text button' ),
				'default' => 'Learn More'
			),
			array(
				'id'      => 'services-info-btnlink-two',
				'type'    => 'text',
				'title'   => __( 'Link button' ),
				'default' => '#box2'
			),
			array(
			    'id'     => 'section-end',
			    'type'   => 'section',
			    'indent' => false,
			),
			array(
			   'id' => 'section-start',
			   'type' => 'section',
			   'title' => __('Block three' ),
			   'indent' => true 
			),
			array(
				'id'      => 'services-info-title-three',
				'type'    => 'text',
				'title'   => __( 'Title' ),
				'default' => 'Web Development'
			),
			array(
				'id'      => 'services-info-description-three',
				'type'    => 'textarea',
				'title'   => __( 'Description' ),
				'default' => 'Websites die converteren en klanten opleveren. Geen dubbel werk door onze slimme integratietools.' 
			),
			array(
				'id'      => 'services-info-btn-three',
				'type'    => 'text',
				'title'   => __( 'Text button' ),
				'default' => 'Learn More'
			),
			array(
				'id'      => 'services-info-btnlink-three',
				'type'    => 'text',
				'title'   => __( 'Link button' ),
				'default' => '#box3'
			),
			array(
			    'id'     => 'section-end',
			    'type'   => 'section',
			    'indent' => false,
			),
			array(
				'id' => 'box-one-options',
				'accordion'     => true,
				'type'          => 'repeatable_list',
				'title'         => 'Box One',
				'items_title'   => __( 'Box' ),
				'add_button'    => __( 'Add New' ),
				'remove_button' => __( 'Remove' ),
				'fields' => array(
					array(
						'id' => 'box-one-title',
						'type'    => 'text',
						'title'   => __( 'Title' ),
					),
					array(
						'id' => 'box-one-description',
						'type'    => 'textarea',
						'title'   => __( 'Description' ),
					),
					array(
						'id' => 'box-one-img',
						'type'    => 'media',
						'title'   => __( 'Picture' ),
					),
					array(
						'id' => 'rbox-one-clear',
						'type'    => 'text',
						'title'   => __( 'Clear' ),
					),
					array(
						'id' => 'rbox-one-title',
						'type'    => 'text',
						'title'   => __( 'Title' ),
					),
					array(
						'id' => 'rbox-one-description',
						'type'    => 'textarea',
						'title'   => __( 'Description' ),
					),
					array(
						'id' => 'rbox-one-img',
						'type'    => 'media',
						'title'   => __( 'Picture' ),
					),
					array(
						'id' => 'rbox-one-clear',
						'type'    => 'text',
						'title'   => __( 'Clear' ),
					),
				)
			),
			array(
				'id' => 'box-two-options',
				'accordion'     => true,
				'type'          => 'repeatable_list',
				'title'         => 'Box Two',
				'items_title'   => __( 'Box' ),
				'add_button'    => __( 'Add New' ),
				'remove_button' => __( 'Remove' ),
				'fields' => array(
					array(
						'id' => 'box-two-title',
						'type'    => 'text',
						'title'   => __( 'Title' ),
					),
					array(
						'id' => 'box-two-description',
						'type'    => 'textarea',
						'title'   => __( 'Description' ),
					),
					array(
						'id' => 'box-two-img',
						'type'    => 'media',
						'title'   => __( 'Picture' ),
					),
					array(
						'id' => 'box-two-clear',
						'type'    => 'text',
						'title'   => __( 'Clear' ),
					),
					array(
						'id' => 'rbox-two-title',
						'type'    => 'text',
						'title'   => __( 'Title' ),
					),
					array(
						'id' => 'rbox-two-description',
						'type'    => 'textarea',
						'title'   => __( 'Description' ),
					),
					array(
						'id' => 'rbox-two-img',
						'type'    => 'media',
						'title'   => __( 'Picture' ),
					),
					array(
						'id' => 'rbox-two-clear',
						'type'    => 'text',
						'title'   => __( 'Clear' ),
					),
				)
			),
			array(
				'id' => 'box-three-options',
				'accordion'     => true,
				'type'          => 'repeatable_list',
				'title'         => 'Box Three',
				'items_title'   => __( 'Box' ),
				'add_button'    => __( 'Add New' ),
				'remove_button' => __( 'Remove' ),
				'fields' => array(
					array(
						'id' => 'box-three-title',
						'type'    => 'text',
						'title'   => __( 'Title' ),
					),
					array(
						'id' => 'box-three-description',
						'type'    => 'textarea',
						'title'   => __( 'Description' ),
					),
					array(
						'id' => 'box-three-img',
						'type'    => 'media',
						'title'   => __( 'Picture' ),
					),
					array(
						'id' => 'box-three-clear',
						'type'    => 'text',
						'title'   => __( 'Clear' ),
					),
					array(
						'id' => 'rbox-three-title',
						'type'    => 'text',
						'title'   => __( 'Title' ),
					),
					array(
						'id' => 'rbox-three-description',
						'type'    => 'textarea',
						'title'   => __( 'Description' ),
					),
					array(
						'id' => 'rbox-three-img',
						'type'    => 'media',
						'title'   => __( 'Picture' ),
					),
					array(
						'id' => 'rbox-three-clear',
						'type'    => 'text',
						'title'   => __( 'Clear' ),
					),
				)
			),
		)
	);

	$metaAboutus = array(
		'title' => 'About us section',
		'icon_class'    => 'icon-large',
		'icon'          => 'el-icon-list-alt',
		'fields' => array(
			array(
				'id'      => 'aboutus-title',
				'type'    => 'textarea',
				'title'   => __( 'Title' ),
				'default' => 'Waarom Kiezen Voor BRIGHTCAMPAIGNS?'
			),
			array(
				'id'      => 'aboutus-background',
				'type'    => 'media',
				'title'   => __( 'Bacground picture' ),
			),
			array(
			   'id' => 'section-start',
			   'type' => 'section',
			   'title' => __('Left block' ),
			   'indent' => true 
			),
			array(
				'id'      => 'aboutus-title-left',
				'type'    => 'text',
				'title'   => __( 'Title' ),
				'default' => 'Waarom Kiezen Voor BRIGHTCAMPAIGNS?'
			),
			array(
				'id' => 'aboutus-options',
				'accordion'     => true,
				'type'          => 'repeatable_list',
				'title'         => 'Skills Block',
				'items_title'   => __( 'Our Skills' ),
				'add_button'    => __( 'Add New' ),
				'remove_button' => __( 'Remove' ),
				'fields' => array(
					array(
						'id' => 'skills-title',
						'type'    => 'text',
						'title'   => __( 'Title' ),
					),
					array(
						'id' => 'skills-description',
						'type'    => 'textarea',
						'title'   => __( 'Description' ),
					),
					array(
						'id' => 'skills-duration',
						'type'        => 'text',
						'validate'    => 'numeric',
						'msg'         => 'custom error message',
						'title'       => __( 'Duration' ),
						'placeholder' => '1000',
						'default'     => '1000'
					),
					array(
						'id' => 'skills-delay',
						'type'        => 'text',
						'validate'    => 'numeric',
						'msg'         => 'custom error message',
						'title'       => __( 'Delay' ),
						'placeholder' => '100',
						'default'     => '300'
					),
					array(
						'id' => 'skills-value',
						'type'        => 'text',
						'validate'    => 'numeric',
						'msg'         => 'custom error message',
						'title'       => __( 'Value' ),
						'placeholder' => '100',
						'default'     => '100'
					),
				)
			),
			array(
			    'id'     => 'section-end',
			    'type'   => 'section',
			    'indent' => false,
			),
			array(
			   'id' => 'section-start',
			   'type' => 'section',
			   'title' => __('Right block' ),
			   'indent' => true 
			),
			array(
				'id'      => 'aboutus-title-right',
				'type'    => 'text',
				'title'   => __( 'Title' ),
				'default' => 'Probeer ons vandaag nog... Gratis!'
			),
			array(
				'id'      => 'aboutus-description-right',
				'type'    => 'textarea',
				'title'   => __( 'Desciption' ),
				'default' => 'Wij vinden het leuk je te helpen. Vul het formulier in of bel ons met je vragen.'
			),
			array(
			    'id'     => 'section-end',
			    'type'   => 'section',
			    'indent' => false,
			),
		)
	);

	$metaPortfolio = array(
		'title' => 'Portfolio section',
		'icon_class'    => 'icon-large',
		'icon'          => 'el-icon-list-alt',
		'fields' => array(
			array(
				'id'      => 'portfolio-title',
				'type'    => 'textarea',
				'title'   => __( 'Title' ),
				'default' => 'INFO FOR YOUR WEBSITES, MARKETING etc...'
			),
			array(
				'id'      => 'portfolio-description',
				'type'    => 'textarea',
				'title'   => __( 'Description' ),
				'default' => 'placeholder placeholder'
			),
		)
	);

	$metaTeam = array(
		'title' => 'Team section',
		'icon_class'    => 'icon-large',
		'icon'          => 'el-icon-list-alt',
		'fields' => array(
			array(
				'id'      => 'team-title',
				'type'    => 'textarea',
				'title'   => __( 'Title' ),
				'default' => 'The Team //need info'
			),
			array(
				'id'      => 'team-description',
				'type'    => 'textarea',
				'title'   => __( 'Description' ),
				'default' => 'NEED INFO'
			),
			array(
				'id' => 'team-options',
				'accordion'     => true,
				'type'          => 'repeatable_list',
				'title'         => 'Team Block',
				'items_title'   => __( 'Features' ),
				'add_button'    => __( 'Add New' ),
				'remove_button' => __( 'Remove' ),
				'fields' => array(
					array(
						'id' => 'team-picture',
						'type'    => 'media',
						'title'   => __( 'Photo' ),
					),
					array(
						'id' => 'team-name',
						'type'    => 'text',
						'title'   => __( 'Name' ),
					),
					array(
						'id' => 'team-details',
						'type'    => 'text',
						'title'   => __( 'Details' ),
					),
					array(
						'id' => 'team-text',
						'type'    => 'text',
						'title'   => __( 'Text' ),
					),
					array(
						'id' => 'team-facebook',
						'type'    => 'text',
						'title'   => __( 'Facebook link' ),
					),
					array(
						'id' => 'team-twitter',
						'type'    => 'text',
						'title'   => __( 'Twitter link' ),
					),
					array(
						'id' => 'team-linkedin',
						'type'    => 'text',
						'title'   => __( 'Linkedin link' ),
					),
					array(
						'id' => 'team-dribbble',
						'type'    => 'text',
						'title'   => __( 'Dribbble link' ),
					),
					array(
						'id' => 'team-rss',
						'type'    => 'text',
						'title'   => __( 'Rss link' ),
					),
					array(
						'id' => 'team-delay',
						'type'    => 'text',
						'title'   => __( 'Delay' ),
					),
				)
			)
		)
	);

	$metaFeatures = array(
		'title' => 'Features section',
		'icon_class'    => 'icon-large',
		'icon'          => 'el-icon-list-alt',
		'fields' => array(
			array(
				'id'      => 'features-img-user',
				'type'    => 'text',
				'title'   => __( 'Picture user' ),
				'default' => 'fa fa-user'
			),
			array(
				'id'      => 'features-timer-user',
				'type'    => 'text',
				'title'   => __( 'Timer user' ),
			),
			array(
				'id'      => 'features-description-user',
				'type'    => 'text',
				'title'   => __( 'Description user' ),
			),
			array(
				'id'      => 'features-img-desktop',
				'type'    => 'text',
				'title'   => __( 'Picture desktop' ),
				'default' => 'fa fa-desktop'
			),
			array(
				'id'      => 'features-timer-desktop',
				'type'    => 'text',
				'title'   => __( 'Timer desktop' )
			),
			array(
				'id'      => 'features-description-desktop',
				'type'    => 'text',
				'title'   => __( 'Description desktop' )
			),
			array(
				'id'      => 'features-img-trophy',
				'type'    => 'text',
				'title'   => __( 'Picture trophy' ),
				'default' => 'fa fa-trophy'
			),
			array(
				'id'      => 'features-timer-trophy',
				'type'    => 'text',
				'title'   => __( 'Timer trophy' )
			),
			array(
				'id'      => 'features-description-trophy',
				'type'    => 'text',
				'title'   => __( 'Description trophy' )
			),
			array(
				'id'      => 'features-img-comment',
				'type'    => 'text',
				'title'   => __( 'Picture comment' ),
				'default' => 'fa fa-comment'
			),
			array(
				'id'      => 'features-timer-comment',
				'type'    => 'text',
				'title'   => __( 'Timer comment' )
			),
			array(
				'id'      => 'features-description-comment',
				'type'    => 'text',
				'title'   => __( 'Description comment' )
			),
			array(
				'id'      => 'features-background',
				'type'    => 'media',
				'title'   => __( 'Bacground picture' )
			),
		)
	);

	$metaPricing = array(
		'title' => 'Pricing section',
		'icon_class'    => 'icon-large',
		'icon'          => 'el-icon-list-alt',
		'fields' => array(
			array(
				'id'      => 'pricing-title',
				'type'    => 'text',
				'title'   => __( 'Title' ),
				'default' => 'E-MAIL MARKETING TARIEVEN'
			),
			array(
				'id'      => 'pricing-description',
				'type'    => 'textarea',
				'title'   => __( 'Description' ),
				'default' => 'E-mail marketing tarieven worden berekend op basis van hoeveel mailtjes u per maand wilt versturen.'
			),
			array(
			   'id' => 'section-start',
			   'type' => 'section',
			   'title' => __('Starter Table Options' ),
			   'indent' => true 
			),
			array(
				'id' => 'table-title-starter',
				'type'    => 'text',
				'title'   => __( 'Title starter' ),
			),
			array(
				'id' => 'table-price-starter',
				'type'    => 'text',
				'title'   => __( 'Price starter' ),
			),
			array(
				'id' => 'table-limitation-starter',
				'type'    => 'text',
				'title'   => __( 'Limitation starter' ),
			),
			array(
				'id' => 'single-table-starter',
				'accordion'     => true,
				'type'          => 'repeatable_list',
				'title'         => 'Starter Block',
				'items_title'   => __( 'Features' ),
				'add_button'    => __( 'Add New' ),
				'remove_button' => __( 'Remove' ),
				'fields' => array(
					array(
						'id' => 'single-table-starter-link',
						'type'    => 'text',
						'title'   => __( 'List' ),
					),
				)
			),
			array(
			    'id'     => 'section-end',
			    'type'   => 'section',
			    'indent' => false,
			),
			array(
			   'id' => 'section-start',
			   'type' => 'section',
			   'title' => __('Basic Table Options' ),
			   'indent' => true 
			),
			array(
				'id' => 'table-title-basic',
				'type'    => 'text',
				'title'   => __( 'Title basic' ),
			),
			array(
				'id' => 'table-prices-basic',
				'type'    => 'text',
				'title'   => __( 'Price basic' ),
			),
			array(
				'id' => 'table-limitation-basic',
				'type'    => 'text',
				'title'   => __( 'Limitation basic' ),
			),
			array(
				'id' => 'single-table-basic',
				'accordion'     => true,
				'type'          => 'repeatable_list',
				'title'         => 'Basic Block',
				'items_title'   => __( 'Features' ),
				'add_button'    => __( 'Add New' ),
				'remove_button' => __( 'Remove' ),
				'fields' => array(
					array(
						'id' => 'single-table-basic-link',
						'type'    => 'text',
						'title'   => __( 'List' ),
					),
				)
			),
			array(
			    'id'     => 'section-end',
			    'type'   => 'section',
			    'indent' => false,
			),
			array(
			   'id' => 'section-start',
			   'type' => 'section',
			   'title' => __('Pro Table Options' ),
			   'indent' => true 
			),
			array(
				'id' => 'table-title-pro',
				'type'    => 'text',
				'title'   => __( 'Title pro' ),
			),
			array(
				'id' => 'table-price-pro',
				'type'    => 'text',
				'title'   => __( 'Price pro' ),
			),
			array(
				'id' => 'table-limitation-pro',
				'type'    => 'text',
				'title'   => __( 'Limitation pro' ),
			),
			array(
				'id' => 'single-table-pro',
				'accordion'     => true,
				'type'          => 'repeatable_list',
				'title'         => 'Pro Block',
				'items_title'   => __( 'Features' ),
				'add_button'    => __( 'Add New' ),
				'remove_button' => __( 'Remove' ),
				'fields' => array(
					array(
						'id' => 'single-table-pro-link',
						'type'    => 'text',
						'title'   => __( 'List' ),
					),
				)
			),
			array(
			    'id'     => 'section-end',
			    'type'   => 'section',
			    'indent' => false,
			),
		)
	);

	$metaPricingtwo = array(
		'title' => 'Pricing section two',
		'icon_class'    => 'icon-large',
		'icon'          => 'el-icon-list-alt',
		'fields' => array(
			array(
				'id'      => 'two-pricing-title',
				'type'    => 'text',
				'title'   => __( 'Title' ),
				'default' => 'MARKETING AUTOMATION TARIEVEN'
			),
			array(
				'id'      => 'two-pricing-description',
				'type'    => 'textarea',
				'title'   => __( 'Description' ),
				'default' => 'Marketing Automation tarieven worden berekend op basis van het aantal contactpersonen dat u heeft.'
			),
			array(
			   'id' => 'section-start',
			   'type' => 'section',
			   'title' => __('One Table Options' ),
			   'indent' => true 
			),
			array(
				'id' => 'table-title-one',
				'type'    => 'text',
				'title'   => __( 'Title' ),
			),
			array(
				'id' => 'table-price-one',
				'type'    => 'text',
				'title'   => __( 'Price' ),
			),
			array(
				'id' => 'table-limitation-one',
				'type'    => 'text',
				'title'   => __( 'Limitation' ),
			),
			array(
				'id' => 'single-table-one',
				'accordion'     => true,
				'type'          => 'repeatable_list',
				'title'         => 'Table One Block',
				'items_title'   => __( 'Features' ),
				'add_button'    => __( 'Add New' ),
				'remove_button' => __( 'Remove' ),
				'fields' => array(
					array(
						'id' => 'single-table-one-link',
						'type'    => 'text',
						'title'   => __( 'List' ),
					),
				)
			),
			array(
			    'id'     => 'section-end',
			    'type'   => 'section',
			    'indent' => false,
			),
			array(
			   'id' => 'section-start',
			   'type' => 'section',
			   'title' => __('Two Table Options' ),
			   'indent' => true 
			),
			array(
				'id' => 'table-title-two',
				'type'    => 'text',
				'title'   => __( 'Title' ),
			),
			array(
				'id' => 'table-price-basic',
				'type'    => 'text',
				'title'   => __( 'Price' ),
			),
			array(
				'id' => 'table-limitation-two',
				'type'    => 'text',
				'title'   => __( 'Limitation' ),
			),
			array(
				'id' => 'single-table-two',
				'accordion'     => true,
				'type'          => 'repeatable_list',
				'title'         => 'Table Two Block',
				'items_title'   => __( 'Features' ),
				'add_button'    => __( 'Add New' ),
				'remove_button' => __( 'Remove' ),
				'fields' => array(
					array(
						'id' => 'single-table-two-link',
						'type'    => 'text',
						'title'   => __( 'List' ),
					),
				)
			),
			array(
			    'id'     => 'section-end',
			    'type'   => 'section',
			    'indent' => false,
			),
			array(
			   'id' => 'section-start',
			   'type' => 'section',
			   'title' => __('Three Table Options' ),
			   'indent' => true 
			),
			array(
				'id' => 'table-title-three',
				'type'    => 'text',
				'title'   => __( 'Title' ),
			),
			array(
				'id' => 'table-price-three',
				'type'    => 'text',
				'title'   => __( 'Price' ),
			),
			array(
				'id' => 'table-limitation-three',
				'type'    => 'text',
				'title'   => __( 'Limitation' ),
			),
			array(
				'id' => 'single-table-three',
				'accordion'     => true,
				'type'          => 'repeatable_list',
				'title'         => 'Table Three Block',
				'items_title'   => __( 'Features' ),
				'add_button'    => __( 'Add New' ),
				'remove_button' => __( 'Remove' ),
				'fields' => array(
					array(
						'id' => 'single-table-three-link',
						'type'    => 'text',
						'title'   => __( 'List' ),
					),
				)
			),
			array(
			    'id'     => 'section-end',
			    'type'   => 'section',
			    'indent' => false,
			),
			array(
			   'id' => 'section-start',
			   'type' => 'section',
			   'title' => __('Four Table Options' ),
			   'indent' => true 
			),
			array(
				'id' => 'table-title-four',
				'type'    => 'text',
				'title'   => __( 'Title' ),
			),
			array(
				'id' => 'table-price-four',
				'type'    => 'text',
				'title'   => __( 'Price' ),
			),
			array(
				'id' => 'table-limitation-four',
				'type'    => 'text',
				'title'   => __( 'Limitation' ),
			),
			array(
				'id' => 'single-table-four',
				'accordion'     => true,
				'type'          => 'repeatable_list',
				'title'         => 'Table Four Block',
				'items_title'   => __( 'Features' ),
				'add_button'    => __( 'Add New' ),
				'remove_button' => __( 'Remove' ),
				'fields' => array(
					array(
						'id' => 'single-table-four-link',
						'type'    => 'text',
						'title'   => __( 'List' ),
					),
				)
			),
			array(
			    'id'     => 'section-end',
			    'type'   => 'section',
			    'indent' => false,
			),
		)
	);

	$metaPricingthree = array(
		'title' => 'Pricing section three',
		'icon_class'    => 'icon-large',
		'icon'          => 'el-icon-list-alt',
		'fields' => array(
			array(
				'id'      => 'three-pricing-title',
				'type'    => 'text',
				'title'   => __( 'Title' ),
				'default' => 'MARKETING AUTOMATION TARIEVEN'
			),
			array(
				'id'      => 'three-pricing-description',
				'type'    => 'textarea',
				'title'   => __( 'Description' ),
				'default' => 'Marketing Automation tarieven worden berekend op basis van het aantal contactpersonen dat u heeft.'
			),
			array(
			   'id' => 'section-start',
			   'type' => 'section',
			   'title' => __('One Table Options' ),
			   'indent' => true 
			),
			array(
				'id' => 'three-table-title-one',
				'type'    => 'text',
				'title'   => __( 'Title' ),
			),
			array(
				'id' => 'three-table-price-one',
				'type'    => 'text',
				'title'   => __( 'Price' ),
			),
			array(
				'id' => 'three-table-limitation-one',
				'type'    => 'text',
				'title'   => __( 'Limitation' ),
			),
			array(
				'id' => 'three-single-table-one',
				'accordion'     => true,
				'type'          => 'repeatable_list',
				'title'         => 'Table One Block',
				'items_title'   => __( 'Features' ),
				'add_button'    => __( 'Add New' ),
				'remove_button' => __( 'Remove' ),
				'fields' => array(
					array(
						'id' => 'three-single-table-one-link',
						'type'    => 'text',
						'title'   => __( 'List' ),
					),
				)
			),
			array(
			    'id'     => 'section-end',
			    'type'   => 'section',
			    'indent' => false,
			),
			array(
			   'id' => 'section-start',
			   'type' => 'section',
			   'title' => __('Two Table Options' ),
			   'indent' => true 
			),
			array(
				'id' => 'three-table-title-two',
				'type'    => 'text',
				'title'   => __( 'Title' ),
			),
			array(
				'id' => 'three-table-price-basic',
				'type'    => 'text',
				'title'   => __( 'Price' ),
			),
			array(
				'id' => 'three-table-limitation-two',
				'type'    => 'text',
				'title'   => __( 'Limitation' ),
			),
			array(
				'id' => 'three-single-table-two',
				'accordion'     => true,
				'type'          => 'repeatable_list',
				'title'         => 'Table Two Block',
				'items_title'   => __( 'Features' ),
				'add_button'    => __( 'Add New' ),
				'remove_button' => __( 'Remove' ),
				'fields' => array(
					array(
						'id' => 'three-single-table-two-link',
						'type'    => 'text',
						'title'   => __( 'List' ),
					),
				)
			),
			array(
			    'id'     => 'section-end',
			    'type'   => 'section',
			    'indent' => false,
			),
			array(
			   'id' => 'section-start',
			   'type' => 'section',
			   'title' => __('Three Table Options' ),
			   'indent' => true 
			),
			array(
				'id' => 'three-table-title-three',
				'type'    => 'text',
				'title'   => __( 'Title' ),
			),
			array(
				'id' => 'three-table-price-three',
				'type'    => 'text',
				'title'   => __( 'Price' ),
			),
			array(
				'id' => 'three-table-limitation-three',
				'type'    => 'text',
				'title'   => __( 'Limitation' ),
			),
			array(
				'id' => 'three-single-table-three',
				'accordion'     => true,
				'type'          => 'repeatable_list',
				'title'         => 'Table Three Block',
				'items_title'   => __( 'Features' ),
				'add_button'    => __( 'Add New' ),
				'remove_button' => __( 'Remove' ),
				'fields' => array(
					array(
						'id' => 'three-single-table-three-link',
						'type'    => 'text',
						'title'   => __( 'List' ),
					),
				)
			),
			array(
			    'id'     => 'section-end',
			    'type'   => 'section',
			    'indent' => false,
			),
			array(
			   'id' => 'section-start',
			   'type' => 'section',
			   'title' => __('Four Table Options' ),
			   'indent' => true 
			),
			array(
				'id' => 'three-table-title-four',
				'type'    => 'text',
				'title'   => __( 'Title' ),
			),
			array(
				'id' => 'three-table-price-four',
				'type'    => 'text',
				'title'   => __( 'Price' ),
			),
			array(
				'id' => 'three-table-limitation-four',
				'type'    => 'text',
				'title'   => __( 'Limitation' ),
			),
			array(
				'id' => 'three-single-table-four',
				'accordion'     => true,
				'type'          => 'repeatable_list',
				'title'         => 'Table Four Block',
				'items_title'   => __( 'Features' ),
				'add_button'    => __( 'Add New' ),
				'remove_button' => __( 'Remove' ),
				'fields' => array(
					array(
						'id' => 'three-single-table-four-link',
						'type'    => 'text',
						'title'   => __( 'List' ),
					),
				)
			),
			array(
			    'id'     => 'section-end',
			    'type'   => 'section',
			    'indent' => false,
			),
		)
	);

	$metaTwitter = array(
		'title' => 'Twitter carousel section',
		'icon_class'    => 'icon-large',
		'icon'          => 'el-icon-list-alt',
		'fields' => array(
			array(
				'id'      => 'twitter-title',
				'type'    => 'textarea',
				'title'   => __( 'Title' ),
				'default' => 'Bedrijven die al met de software werken!'
			),
			array(
				'id' => 'twitter-carousel-option',
				'accordion'     => true,
				'type'          => 'repeatable_list',
				'title'         => 'Twitter Carousel Block',
				'items_title'   => __( 'Features' ),
				'add_button'    => __( 'Add New' ),
				'remove_button' => __( 'Remove' ),
				'fields' => array(
					array(
						'id' => 'twitter-carousel-img',
						'type'    => 'media',
						'title'   => __( 'List' ),
					),
					array(
						'id' => 'twitter-carousel-img-class',
						'type'    => 'text',
						'title'   => __( 'List-class' ),
						'placeholder' => 'Use only for 1th element = active wow fadeIn'
					),
				)
			)
		)
	);

	$metaContact = array(
		'title' => 'Contact section',
		'icon_class'    => 'icon-large',
		'icon'          => 'el-icon-list-alt',
		'fields' => array(
			array(
				'id'      => 'contact-title',
				'type'    => 'text',
				'title'   => __( 'Title' ),
				'default' => 'Contact Us'
			),
			array(
				'id'      => 'contact-description',
				'type'    => 'text',
				'title'   => __( 'Description' ),
				'default' => 'Wij vinden het leuk je te helpen. Vul het formulier in of bel ons met je vragen.'
			),
			array(
				'id'      => 'contact-background',
				'type'    => 'media',
				'title'   => __( 'Footer logo' ),
			),
			array(
					'id'    => 'footer-mailto',
					'type'  => 'text',
					'title' => __( 'Footer mail link' )
				),
			array(
					'id'    => 'footer-skype',
					'type'  => 'text',
					'title' => __( 'Footer skype link' )
				),
			array(
				'id' => 'contact-right-li',
				'accordion'     => true,
				'type'          => 'repeatable_list',
				'title'         => 'Twitter Carousel Block',
				'items_title'   => __( 'Features' ),
				'add_button'    => __( 'Add New' ),
				'remove_button' => __( 'Remove' ),
				'fields' => array(
					array(
						'id' => 'contact-right-li-fafa',
						'type'    => 'text',
						'title'   => __( 'Fa element' ),
					),
					array(
						'id' => 'contact-right-li-name',
						'type'    => 'text',
						'title'   => __( 'Name of field' ),
					),
					array(
						'id' => 'contact-right-li-text',
						'type'    => 'text',
						'title'   => __( 'Field text' ),
					),
				)
			)
		)
	);

	$page_options[] = $fontstyle;
	$page_options[] = $metaHeader;
	$page_options[] = $metaServices;
	$page_options[] = $metaAboutus;
	$page_options[] = $metaPortfolio;
	$page_options[] = $metaTeam;
	$page_options[] = $metaFeatures;
	$page_options[] = $metaPricing;
	$page_options[] = $metaPricingtwo;
	$page_options[] = $metaPricingthree;
	$page_options[] = $metaTwitter;
	$page_options[] = $metaContact;

	$metaboxes[] = array(
		'id'            => 'page_options',
		'title'         => __( 'Page options', THEME_OPT ),
		'post_types'    => array( 'page' ),
		'page_template' => array('bchome-page.php'),
		'position'      => 'normal', // normal, advanced, side
		'priority'      => 'high', // high, core, default, low
		'sidebar'       => false, // enable/disable the sidebar in the normal/advanced positions
		'sections'      => $page_options,
	);




	$metaText = array(
		'title' => 'Section portfolio',
		'icon_class'    => 'icon-large',
		'icon'          => 'el-icon-list-alt',
		'fields' => array(
			array(
				'id'      => 'folio-class',
				'type'    => 'text',
				'title'   => __( 'Portfolio class for styles' ),
				'placeholder' => 'This field must have a class'
			),
			array(
				'id'      => 'folio-infos-h3',
				'type' => 'text',
			    'title' => __('Portfolio info title')
			),
			array(
				'id' => 'folio-infos-p',
				'accordion'     => true,
				'type'          => 'repeatable_list',
				'title'         => 'Infos paragraph',
				'items_title'   => __( 'Features' ),
				'add_button'    => __( 'Add New' ),
				'remove_button' => __( 'Remove' ),
				'fields' => array(
					array(
						'id' => 'folio-infos-pchild',
						'type'    => 'textarea',
						'title'   => __( 'P element' ),
					),
				)
			),
			array(
				'id'      => 'folio-details-h3',
				'type' => 'text',
			    'title' => __('Portfolio details title')
			),
			array(
				'id' => 'folio-details-p',
				'accordion'     => true,
				'type'          => 'repeatable_list',
				'title'         => 'Details paragraph',
				'items_title'   => __( 'Features' ),
				'add_button'    => __( 'Add New' ),
				'remove_button' => __( 'Remove' ),
				'fields' => array(
					array(
						'id' => 'folio-details-pchild',
						'type'    => 'textarea',
						'title'   => __( 'P element' ),
					),
				)
			),
			
			array(
				'id'      => 'folio-medias',
				'type'    => 'media',
				'title'   => __( 'Portfolio image' ),
			),
		)
	);

	$page_options_portfolio[] = $metaText;
	
	$metaboxes[] = array(
		'id'            => 'page_options_portfolio',
		'title'         => __( 'Page options', THEME_OPT ),
		'post_types'    => array( 'portfolio' ),
		// 'page_template' => array('lgmhome-page.php'),
		'position'      => 'normal', // normal, advanced, side
		'priority'      => 'high', // high, core, default, low
		'sidebar'       => false, // enable/disable the sidebar in the normal/advanced positions
		'sections'      => $page_options_portfolio,
	);

	return $metaboxes;
  }
  add_action("redux/metaboxes/{$redux_opt_name}/boxes", 'redux_add_metaboxes');
endif;
