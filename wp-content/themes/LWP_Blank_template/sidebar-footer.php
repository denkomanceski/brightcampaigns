<!-- sidebar -->
<aside class="sidebar" role="complementary">

	<div class="sidebar-widget-footer">
		<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-area-2')) ?>
	</div>

</aside>
<!-- /sidebar -->