<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'brighton_brightcampaign');

/** MySQL database username */
define('DB_USER', 'brighton_bright');

/** MySQL database password */
define('DB_PASSWORD', 'B1rightcampaign2');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ':/F 4&M!Ta&)=<aF~^LL2(xE]jIbR<m)7c5CHlbq{aYB:J/Wd;IX5Kl.,fdIB64!');
define('SECURE_AUTH_KEY',  '-3${&j3i0s<=`&{BZd^x+iUrp/5*Ro0?RHYD goJ%%$H^6>pX%!t3Z%Rs?wl<wEz');
define('LOGGED_IN_KEY',    '[@kPClFSM^{m3-N ;@0~jV`-(y@6maKvdKs]qWG(M#HKrP mq{Y=&VG.w/LWar$)');
define('NONCE_KEY',        '& }|QKG-Aqb#<K^Y~@.s}A+5k`)Ig-LhkzgT7QjAQAX,r*oGap{{zgUc4G8 t7>m');
define('AUTH_SALT',        ',Dp*x]z6z:6MpP>{ka<at1kE`7#Dw_l.LFvO<*lu]S<$N}%.HO9~ 2[Y:I,PGNQ[');
define('SECURE_AUTH_SALT', '):)W!N&scq9od3,A=L`~Tq/Ka_%0!F9;.A%4VRj}ugpI]p*|<9c4cYr+3OxO Zij');
define('LOGGED_IN_SALT',   'Zsv>j+E^m^J5hK{;7f2E94;V:]@#[1{#MQhZf6Y!FOSy8^8Ja6~l$L;W[B4xs01l');
define('NONCE_SALT',       'A*KR)ULG/g&)2jNC*H&hDnO9]s<UmpxDX<81c`K6DJCM#ICW;%UZ}gEIncmz1Re1');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
